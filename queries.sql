-- Consultas

-- 1. Empleados que sean mujeres cuyo sueldo sea mayor a 3.000.000.
SELECT nombres, apellidos FROM empleados WHERE genero='F' and sueldo > 3000000;
-- 2. Cantidad de hijos varones para el empleado con cédula 12345.
SELECT COUNT(id_hijo_empleado) FROM hijos_empleados WHERE genero='M' and documento_padre = 12345;
-- 3. Nombre de los empleados que pertenecen a la dependencia de Sistemas.
SELECT nombres, apellidos FROM proyectos WHERE nombre_dependencia = 'Sistemas';
-- 4. Cantidad de empleados para cada una de las dependencias.
SELECT COUNT(DISTINCT nombre_dependencia) FROM empleados;
-- 5. Nombre del proyecto y nombre de la dependincia a la que pertenece.
SELECT nombre_proyecto, nombre_dependencia FROM proyectos;
-- 6. Cantidad de proyectos actuales asignados a Pedro Pérez.
SELECT COUNT(id_proyecto) FROM proyectos WHERE encargado = 'Pedro P%rez';
-- 7. Nombre de los Proyectos que finalizan dentro de 7 días.
SELECT nombre_proyecto FROM proyectos WHERE fecha_finaliacion = now() + 7;
-- 8. Nombre de los proyectos atrazados.
SELECT nombre_proyecto FROM proyectos WHERE fecha_finaliacion < now();
-- 9. Dependencias con menos de 3 empleados.
SELECT COUNT(nombre_empleado)  < 3 FROM dependencias;